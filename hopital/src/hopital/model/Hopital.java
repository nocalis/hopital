package hopital.model;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Hopital {

	private static Hopital hp;
	private List<Patient> fileDAttente;
	// private Secretaire secretaire;

	private Hopital() {
		
		this.fileDAttente = new ArrayList<>(
				
//				Arrays.asList( 
//						
//						new Patient("3", "Doe", "John", "066789566", "85 rue marceau montreuil"),
//						new Patient("3", "Dovrfe", "Jtrhn", "066789566", "85 rue marceau montreuil"),
//						new Patient("2", "Dtye", "John", "066789566", "85 rue de charonne Paris"),
//						new Patient("1", "DUI", "juijh", "066789566", "82 rue de Paris Paris"))
				
				);

	}

	public List<Patient> getFileDAttente() {
		return fileDAttente;
	}

	public void setFileDAttente(List<Patient> fileDAttente) {
		this.fileDAttente = fileDAttente;
	}

	public static Hopital getHp() {
		return hp;
	}

	public static Hopital getInstance() {
		if (hp == null)
			return hp = new Hopital();
		else
			return hp;

	}

	@Override
	public String toString() {
		return "Hopital [fileDAttente=" + fileDAttente + "]";
	}
	
	

}
