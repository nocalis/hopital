package hopital.model;



public class Authentification {
	
	private int id;
	private String login;
	private String password;
	private String nom;
	private int metier;
	private int numSalle;
	
	
	
	public int getNumSalle() {
		return numSalle;
	}

	public int getId() {
		return id;
	}

	public String getLogin() {
		return login;
	}

	public String getPassword() {
		return password;
	}

	public String getNom() {
		return nom;
	}

	public int getMetier() {
		return metier;
	}

	public Authentification(String login, String password) {
		this.login = login;
		this.password = password;
	}
	
	public Authentification(int id, String login, String password, String nom, int metier, int numSalle) {
		this.id = id;
		this.login = login;
		this.password = password;
		this.nom = nom;
		this.metier = metier;
		this.numSalle = numSalle;
	}

	@Override
	public String toString() {
		return "Authentification [id=" + id + ", login=" + login + ", password=" + password + ", nom=" + nom
				+ ", metier=" + metier + "]";
	}
	
	

	
	
	
	
	
	
	
	

}
