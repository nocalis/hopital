package hopital.model;

import java.util.List;

public class Salle {

	private int id;
	private List<Medecin> listeMedecin;
	private Patient patient;

	public Salle(int id, List<Medecin> listeMedecin, Patient patient) {
		super();
		this.id = id;
		this.listeMedecin = listeMedecin;
		this.patient = patient;
	}

	@Override
	public String toString() {
		return "Salle [id=" + id + ", listeMedecin=" + listeMedecin + ", patient=" + patient + "]";
	}

	public int getId() {
		return id;
	}

	public List<Medecin> getListeMedecin() {
		return listeMedecin;
	}

	public Patient getPatient() {
		return patient;
	}
	
	
	
	

}
