package hopital.model;

import java.sql.SQLException;

public class Secretaire extends PersonnelHospitalier {
	String nom;
	
	
	public Secretaire(Authentification auth) throws ClassNotFoundException, SQLException {
		super(auth);
		
		Patient p1 = new Patient("1", "Jeanne", "BBB", "0102030405", "AAA"); 
        
        SecretaireDAO s1 = new SecretaireDAO();
        
        //System.out.println("s " + s1.rechercherPatient(p1));
	}

	public Secretaire(Authentification auth, String nom) {
		super(auth);
		this.nom = nom;
	}
	
	public String getNom() {
		return nom;
	}

	@Override
	public String toString() {
		return "Secretaire [nom=" + nom + "]";
	}
	

}
