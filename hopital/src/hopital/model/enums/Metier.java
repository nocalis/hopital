package hopital.model.enums;

public enum Metier {

	SECRETAIRE(0), MEDECIN(1);

	private int num;

	private Metier(int num) {

		this.num = num;

	}

	public int getNum() {
		return num;
	}

	public void setNum(int num) {
		this.num = num;
	}
	
	@Override
	public String toString(){
		return name() + " , num: " + this.getNum();
	}

}
