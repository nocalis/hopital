package hopital.model;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

public class PatientDao {

    public Patient selectById(long patientId) throws ClassNotFoundException, SQLException {
        Class.forName("com.mysql.jdbc.Driver");
        Patient patient = null;
        Connection conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/hopital-apside", "root", "root");

        String sql = "select * from patients where id=" + patientId;
        Statement st = conn.createStatement();
        ResultSet rs = st.executeQuery(sql);
        if (rs.next()) {
            patient = new Patient(rs.getString("id"), rs.getString("nom"), rs.getString("prenom"),
                    rs.getString("telephone"), rs.getString("adresse"));
        }
        conn.close();
        return patient;
    }

    public ArrayList<Patient> select() throws ClassNotFoundException, SQLException {
        Class.forName("com.mysql.jdbc.Driver");

        ArrayList<Patient> liste = new ArrayList<Patient>();

        Connection conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/hopital-apside", "root", "root");

        String sql = "select * from patients";
        Statement st = conn.createStatement();
        ResultSet rs = st.executeQuery(sql);
        while (rs.next()) {
            Patient p = new Patient();
            p.setId(rs.getString("id"));
            p.setNom(rs.getString("nom"));
            p.setPrenom(rs.getString("prenom"));
            p.setTelephone(rs.getString("telephone"));
            p.setAdresse(rs.getString("adresse"));
            liste.add(p);
        }
        conn.close();
        return liste;
    }

    public void updatePatient(Patient p) throws ClassNotFoundException, SQLException {
        Class.forName("com.mysql.jdbc.Driver");

        Connection conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/hopital-apside", "root", "root");

        String sql = "update personnes set nom='" + p.getNom() + "',prenom='" + p.getPrenom() + "',telephone='" + p.getTelephone()+ "', adresse='" + p.getAdresse()
                + " where id= " + p.getId();
        Statement st = conn.createStatement();
        st.executeUpdate(sql);

        conn.close();

    }

    public void insert(Patient pa) throws ClassNotFoundException, SQLException {
        Class.forName("com.mysql.jdbc.Driver");

        Connection conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/hopital-apside", "root", "root");

        String sql = "insert into patients values (?,?,?,?,? )";
        PreparedStatement ps = conn.prepareStatement(sql);
        ps.setString(1, pa.getId());
        ps.setString(2, pa.getNom());
        ps.setString(3, pa.getPrenom());
        ps.setString(4, pa.getTelephone());
        ps.setString(5, pa.getAdresse());

        ps.executeUpdate();
        
		System.out.println("succ�s enregistremnt patient ");

        conn.close();

    }

}
