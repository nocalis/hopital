package hopital.model;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class VisiteDAO {

	public boolean insertVisits(List<Visite> visites) throws ClassNotFoundException, SQLException {

		// System.out.println(visites);

		Class.forName("com.mysql.jdbc.Driver");
		Authentification authReturned = null;
		Connection conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/hopital-apside", "root", "root");

		for (int i = 0; i < visites.size(); i++) {

			String sql = "Insert into visites(idpatient, date, idmedecin, `num-salle`, tarif) values(?,?,?,?,?)";
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setLong(1, visites.get(i).getIdPatient());

			ps.setString(2, visites.get(i).getDate());

			ps.setInt(3, visites.get(i).getMedecin().getAuth().getId());
			ps.setInt(4, visites.get(i).getNumSalle());
			ps.setInt(5, visites.get(i).getTarif());

			// System.out.println(sql + "\n");

			ps.executeUpdate();

		}

		conn.close();

		return true;

	}

	public List<String> selectByMedecinId(int medecinId) throws ClassNotFoundException, SQLException {

		Class.forName("com.mysql.jdbc.Driver");
		Authentification authReturned = null;
		Connection conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/hopital-apside", "root", "root");

		String sql = "SELECT p.nom, p.prenom, p.telephone, p.adresse, v.date, v.`num-salle`, v.tarif FROM "
				+ "patients p INNER JOIN visites v ON p.id = v.idpatient WHERE v.idmedecin=" + medecinId;

		// System.out.println(sql);

		Statement st = conn.createStatement();
		ResultSet rs = st.executeQuery(sql);

		// System.out.println(rs.);

		ArrayList<String> visitList = new ArrayList();

		while (rs.next()) {

			String infos = rs.getString("nom") + "\t\t" + rs.getString("prenom") + "\t" + rs.getString("telephone") + "\t"
					+ rs.getString("adresse") + "\t" + rs.getString("date") + "\t" + rs.getInt("num-salle") + "\t"
					+ rs.getString("tarif");

			visitList.add(infos);

		}

		conn.close();

		return visitList;

	}

	public List<String> selectVisitsByPatientId(long patientId) throws ClassNotFoundException, SQLException {

		Class.forName("com.mysql.jdbc.Driver");
		Authentification authReturned = null;
		Connection conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/hopital-apside", "root", "root");

		String sql = "SELECT date, idmedecin, `num-salle`, tarif FROM visites WHERE idpatient=" + patientId;

		// System.out.println(sql);

		Statement st = conn.createStatement();
		ResultSet rs = st.executeQuery(sql);

		// System.out.println(rs);

		ArrayList<String> visitList = new ArrayList();

		while (rs.next()) {

			String infos = rs.getString("date") + "\t" + rs.getInt("idmedecin") + "\t" + rs.getInt("num-salle") + "\t"
					+ rs.getInt("tarif") + "\t";

			// System.out.println(infos);

			visitList.add(infos);

		}

		conn.close();

		return visitList;

	}

}
