package hopital.user;

import java.sql.SQLException;
import java.util.Scanner;

import hopital.model.Authentification;
import hopital.model.AuthentificationDAO;
import hopital.model.Hopital;
import hopital.model.enums.Metier;
import hopital.user.modules.MedecinModule;
import hopital.user.modules.SecretaireModule;

public class GestionnairePatients {

	public static void main(String[] args) throws ClassNotFoundException, SQLException {

		Hopital hp = Hopital.getInstance();

		lancementModule();

	}

	public static void afficheLogo() {

		System.out.println();

	}

	public static void lancementModule() {

		Scanner sc = new Scanner(System.in);

		System.out.println("Bienvenu sur le gestionnaire de patients \n");
		System.out.println("renseignez votre login: \n");

		String login = sc.nextLine();

		System.out.println("mot de passe: \n");

		String mdp = sc.nextLine();

		Authentification utilisateur = new Authentification(login, mdp);

		try {
			AuthentificationDAO dao = new AuthentificationDAO();
			Authentification authReturned = dao.SelectAuthentification(utilisateur);
			// System.out.println(authReturned);

			if (authReturned.getMetier() == Metier.SECRETAIRE.getNum()) {

				SecretaireModule moduleSecretaire = new SecretaireModule(authReturned);

			} else if (authReturned.getMetier() == Metier.MEDECIN.getNum()) {

				MedecinModule moduleMedecin = new MedecinModule(authReturned);

			}

		} catch (Exception e) {
			// TODO: handle exception
			System.out.println(e);
			System.out.println("Personnel non reconnu, acc�s refus� \n");
			lancementModule();
		}

		// secr�taire ou m�decin :

	}

}
